/*==============================================================================*/
/* Programme 	: Tache.c (Escalve)						*/
/* Auteur 	: Daniel CHILLET						*/
/* Date 	: Novermbre 2001						*(
/* 										*/
/* Objectifs	: Une tache qui fait pas grand chose d'autre que d'afficher	*/
/* 		  sur quelle machine elle tourne 				*/
/* 										*/
/* Principe	: Tache est lance par le maitre (LanceTaches) 			*/
/* 										*/
/*==============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include "pvm3.h"
#include <string.h> /* memset */
#include <unistd.h> /* close */

#define MAX_CHAINE 100

#define MAITRE_ENVOI 	0
#define MAITRE_RECOIT	5

#define ESCLAVE_ENVOI	MAITRE_RECOIT
#define ESCLAVE_RECOIT 	MAITRE_ENVOI

#define FOPEN(fich,fichier,sens) 	if ((fich=fopen(fichier,sens)) == NULL) { 				\
						printf("Probleme d'ouverture du fichier %s\n",fichier);		\
						exit(-1);							\
					} 
#define CALLOC(ptr, nr, type) 		if (!(ptr = (type *) calloc((size_t)(nr), sizeof(type)))) {		\
						printf("Erreur lors de l'allocation memoire \n") ; 		\
						exit (-1);							\
					}									\


main(argc, argv)
int argc;
char *argv[];
{
	int i;
	int mytid;
	int tidMaitre;
	int *newvaleur;
	int msgtype;
	char fichierOutput[MAX_CHAINE];
	
	int min, nbPtsPerLine;
	double etalement;

	FILE *OutputFile;
	int *recuDuMaitre;

	wait(2);
	
	mytid = pvm_mytid();
	
	sprintf(fichierOutput, "tmp/Output%d.txt", mytid);

	FOPEN(OutputFile, fichierOutput, "w");
	
	
	fprintf(OutputFile, "Tache %d en attente de reception de message de la part du maitre \n", mytid);
	fflush(OutputFile);
	msgtype = ESCLAVE_RECOIT;
	pvm_recv(-1 , msgtype);
	
	pvm_upkint(&min, 1, 1);
	pvm_upkdouble(&etalement, 1, 1);
	pvm_upkint(&nbPtsPerLine, 1, 1);
	pvm_upkint(&tidMaitre, 1, 1);
	
	fprintf(OutputFile, "Reception des messages : min=%d, nbPtsPerLine=%d, etalement=%f, id maitre=%d.\nenvoi confirmation au maitre.  \n", min, nbPtsPerLine, etalement, tidMaitre);
	fflush(OutputFile);
	
	msgtype = ESCLAVE_ENVOI;
	pvm_initsend(PvmDataDefault);
	pvm_pkint(&mytid, 1, 1);
	pvm_send(tidMaitre, msgtype);
	fprintf(OutputFile, "Confirmation envoyée.\n");
	fflush(OutputFile);
	

//	recuDuMaitre = (int *) malloc(nbPtsPerLine * sizeof(int));
//	newvaleur = (int *) malloc(nbPtsPerLine * sizeof(int));
	// memset(recuDuMaitre, 0, sizeof(nbPtsPerLine * sizeof(int))); // initialiser la ligne
	CALLOC(recuDuMaitre, nbPtsPerLine, int); // Allocation mémoire + initialisation
	CALLOC(newvaleur, nbPtsPerLine, int);
	
	msgtype = ESCLAVE_RECOIT;
	int lineIndex;
	fprintf(OutputFile, "En attente de ligne de la part du maitre...\n\n");
	fflush(OutputFile);
	
	pvm_recv(-1, msgtype);
	pvm_upkint(&lineIndex, 1, 1);
	pvm_upkint(recuDuMaitre, 1, 1);
	
	fprintf(OutputFile, "Données recues du maitre. Démarrage du while.\n");
	fflush(OutputFile);
	
	int sizeofLine = sizeof(nbPtsPerLine * sizeof(int));
	
	while(lineIndex != -1) {
		fprintf(OutputFile, "Reception de la ligne n° %d\nPremière valeur du tableau de données: %d\nTraitement... \n", lineIndex, recuDuMaitre[0]);
		fflush(OutputFile);
		
		for (i = 0 ; i < nbPtsPerLine ; i++) {
			newvaleur[i] = ((recuDuMaitre[i] - min) * etalement);
		}

		msgtype = ESCLAVE_ENVOI;
		pvm_initsend(PvmDataDefault);
		pvm_pkint(&lineIndex, 1, 1);
		pvm_pkint(&mytid, 1, 1);
		pvm_pkint(newvaleur, nbPtsPerLine, 1);
		
		pvm_send(tidMaitre, msgtype);
		fprintf(OutputFile, "Tache : données envoyées au maitre : ligne n° %d\nPremière valeur du tableau de données : %5d \n\n", lineIndex, newvaleur[0]);
		fflush(OutputFile);
		
		// TODO envoyer aussi le temps d'exec
		
		memset(recuDuMaitre, 0, sizeofLine); // initialiser la ligne
		memset(newvaleur, 0, sizeofLine);
		msgtype = ESCLAVE_RECOIT;
		pvm_recv(-1, msgtype);
		pvm_upkint(&lineIndex, 1, 1);
		pvm_upkint(recuDuMaitre, 1, 1);
	}
	fprintf(OutputFile, "Tache : fin d'execution  \n");
	fflush(OutputFile);
	pvm_exit();

	fclose(OutputFile);
	
	free(recuDuMaitre);
	free(newvaleur);

	exit(0); 

}
