/*==============================================================================*/
/* Programme 	: LanceTaches.c	(Maitre)					*/
/* Auteur 	: Daniel CHILLET						*/
/* Date 	: Novembre 2001							*/
/* 										*/
/* Objectifs	: Programme principal permettant de lancer 1 tache par		*/
/*		  host de la machine virtuelle					*/
/* 										*/
/* Principe	: Ce programme est base sur un calcul distribue sur une machine	*/
/*		  PVM. 								*/
/* 										*/
/* Fonctionnemnt: 1) Analyse des hosts de la machine virtuelle			*/
/*		  2) Lancement de 1 tache par host 	 			*/
/*		  3) Liste des taches de la machine PVM	 			*/
/*==============================================================================*/


#include <stdlib.h>
#include <stdio.h>
#include "pvm3.h"
#include <string.h> /* memset */
#include <unistd.h> /* close */
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

#define RESULT_TAG 1
#define MAX_LENGTH 128

#define MAX_HOSTS 100
#define MAX_CHAINE 100
#define MAX_PARAM 10

#define MAITRE_ENVOI 	0
#define MAITRE_RECOIT	5

#define ESCLAVE_ENVOI	MAITRE_RECOIT
#define ESCLAVE_RECOIT 	MAITRE_ENVOI

#define MIN(a, b) 	(a < b ? a : b)
#define MAX(a, b) 	(a > b ? a : b)

#define MAX_VALEUR 	255
#define MIN_VALEUR 	0

#define NBPOINTSPARLIGNES 15
#define false 0
#define true 1
#define boolean int



#define CALLOC(ptr, nr, type) 		if (!(ptr = (type *) calloc((size_t)(nr), sizeof(type)))) {		\
						printf("Erreur lors de l'allocation memoire \n") ; 		\
						exit (-1);							\
					}									\


#define FOPEN(fich,fichier,sens) 	if ((fich=fopen(fichier,sens)) == NULL) { 				\
						printf("Probleme d'ouverture du fichier %s\n",fichier);		\
						exit(-1);							\
					}									\

#define initTimer struct timeval tv1, tv2; struct timezone tz
#define startTimer gettimeofday(&tv1, &tz)
#define stopTimer gettimeofday(&tv2, &tz)
#define tpsCalcul (tv2.tv_sec-tv1.tv_sec)*1000000L + (tv2.tv_usec-tv1.tv_usec)


int EUCLIDEAN(a, b) {
	while (a>0 && b>0 && a!=b) {
		if (a > b) {
			a = a - b;
		} else {
			b = b - a;
		}
	}
	return (a>0) ? a : b;
}
    
main(argc, argv)
int argc;
char *argv[];
{
	int info ;
	int nhost;
	int mytid;
	int who, retour;

	int narch;
	struct pvmhostinfo *hostp;
	struct pvmtaskinfo *taskinfo;
	int numtaches[MAX_HOSTS];
	int nbtaches;
	char *param[MAX_PARAM];

	//char Chemin[MAX_CHAINE];
	char *Chemin;
	char Fichier[MAX_CHAINE];
	
	CALLOC(Chemin, MAX_CHAINE, char);
	Chemin = getenv("PWD");
	//*Chemin = getenv("PWD");
/*	sprintf(Fichier, "%s/Tache",*Chemin);*/
	sprintf(Fichier, "./Tache");

	hostp = calloc(1, sizeof(struct pvmhostinfo));
	taskinfo = calloc(1, sizeof(struct pvmtaskinfo));
	
	int iddaemon ;
	int msgtype; 

	int valeur;
	
	int LE_MIN = MAX_VALEUR;
	int LE_MAX = MIN_VALEUR;
	
	boolean inverse = false;
	double ETALEMENT = 0.0;
	

	mytid = pvm_mytid();

	valeur = mytid;

	int min = LE_MIN;
	
	/* Calcul de l'étalement ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
	// ETALEMENT = (double)(MAX_VALEUR - MIN_VALEUR) / (double)(LE_MAX - LE_MIN);
	
	if (inverse) {
		ETALEMENT = 0.2;	
	} else {
		ETALEMENT = (float)(MAX_VALEUR - MIN_VALEUR) / (float)(LE_MAX - LE_MIN);	
	}
	double etalement = ETALEMENT;
	
	int nbOfPixelsPerLine = NBPOINTSPARLIGNES;
	
	
	/* Autres déclarations pour le code séquentiel~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
	FILE *Src, *Dst;

	char SrcFile[MAX_CHAINE];
	char DstFile[MAX_CHAINE];
	
	char ligne[MAX_CHAINE];
	
	int i, j, x, y, n, P, X, Y;
	
	int **image;
	int **resultat;
	
	
	int lineIndex;
	int lineIndex2;
	int confirmation;
	int remainingHosts;
	
	info = pvm_config(&nhost, &narch, &hostp);

	// printf("Nombre de noeuds dans la Parallel Virtual Machine : %d\n",nhost);
	// printf("Nombre d'architecture dans la Parallel Virtual Machine : %d\n",narch);
	
	// printf("\nListe des machines de la PVM\n");
	for (i=0 ; i < nhost ; i++) {
		//if(hostp[i].hi_tid != hostp[0].hi_tid) {
		// printf("\tNoeud %d : \n", i);
		// printf("\t\t hi_tid = %d \n",hostp[i].hi_tid);
		// printf("\t\t hi_name = %s \n",hostp[i].hi_name);
		// printf("\t\t hi_arch = %s \n",hostp[i].hi_arch);
		// printf("\t\t hi_speed = %d \n",hostp[i].hi_speed);

		param[0] = calloc(1, MAX_CHAINE);
		param[1] = calloc(1, MAX_CHAINE);
		sprintf(param[0],"%d",i);
		sprintf(param[1], "%s",hostp[i].hi_name); 
		param[2] = NULL;
	
		/* Pour lancer la tache sur un host particulier,
		il faut placer le flag PvmTaskHost 
		Sinon, le systeme choisit tout seul le meilleur host
		pour lancer la tache */
		  
      		nbtaches = pvm_spawn(Fichier, &param[0], PvmTaskHost, hostp[i].hi_name,1, &numtaches[i]);
	}
	

	/*========================================================================*/
	/* Boucle sur toutes les lignes de l'image et les envoyer aux slaves	*/
	/*========================================================================*/

	
	/*========================================================================*/
	/* Recuperation des parametres						*/
	/*========================================================================*/

	sscanf(argv[1],"%s", SrcFile);
	
	sprintf(DstFile,"%s.new",SrcFile);
	
	/*========================================================================*/
	/* Recuperation de l'endroit ou l'on travaille			*/
	/*========================================================================*/

	CALLOC(Chemin, MAX_CHAINE, char);
	Chemin = getenv("PWD");
	// printf("Repertoire de travail : %s \n\n",Chemin);
	

	/*========================================================================*/
	/* Ouverture des fichiers						*/
	/*========================================================================*/

	// printf("Operations sur les fichiers\n");

	FOPEN(Src, SrcFile, "r");
	// printf("\t Fichier source ouvert (%s) \n",SrcFile);
		
	FOPEN(Dst, DstFile, "w");
	// printf("\t Fichier destination ouvert (%s) \n",DstFile);
	
	/*========================================================================*/
	/* On effectue la lecture du fichier source */
	/*========================================================================*/
	
	// printf("\t Lecture entete du fichier source ");
	
	for (i = 0 ; i < 2 ; i++) {
		fgets(ligne, MAX_CHAINE, Src);	
		fprintf(Dst,"%s", ligne);
	}	

	fscanf(Src," %d %d\n",&X, &Y);
	fprintf(Dst," %d %d\n", X, Y);
	
	fgets(ligne, MAX_CHAINE, Src);	/* Lecture du 255 	*/
	fprintf(Dst,"%s", ligne);
	
	// printf(": OK \n");
	
	/*========================================================================*/
	/* Allocation memoire pour l'image source et l'image resultat 		*/
	/*========================================================================*/
	
	CALLOC(image, Y+1, int *);
	CALLOC(resultat, Y+1, int *);
	memset(resultat, 0, Y);
	for (i=0;i<Y;i++) {
		CALLOC(image[i], X+1, int);
		CALLOC(resultat[i], X+1, int);
		for (j=0;j<X;j++) {
			image[i][j] = 0;
			resultat[i][j] = 0;
		}
	}
	// printf("\t\t Initialisation de l'image [%d ; %d] : Ok \n", X, Y);
			
	int TailleImage = X * Y;
	
	x = 0;
	y = 0;
	
	// lignes = 0;
	
	/*========================================================================*/
	/* Lecture du fichier pour remplir l'image source 			*/
	/*========================================================================*/
	
	while (! feof(Src)) {
		n = fscanf(Src,"%d",&P);
		image[y][x] = P;	
		LE_MIN = MIN(LE_MIN, P);
		LE_MAX = MAX(LE_MAX, P);
		x ++;
		if (n == EOF || (x == X && y == Y-1)) {
			break;
		}
		if (x == X) {
			x = 0 ;
			y++;
		}
	}
	fclose(Src);
	// printf("\t Lecture du fichier image : Ok \n\n");
	
	
	
	
	
//	for (i=0 ; i < nhost ; i++) {
	// printf("\t (sizeof(image)/nbOfPixelsPerLine)=== %d, Y(nbOfPixelsPerLine)=== %d, X=== %d \n\n", TailleImage, Y,X);
	
	
	
	
	// printf("Valeurs envoyees à toutes les machines de la PVM : min=%d, etalement=%f, nbPtsPerLine=%d, id maitre=%d\n\n", min, etalement, X, valeur);
	
	
	msgtype = MAITRE_ENVOI;
	for (i=0 ; i < nhost ; i++) {
		// printf("\tEnvoi de données depuis LANCETACHES n°%d vers %s : Tache %d %s %s (%d) \n", 
			// mytid, hostp[i].hi_name, numtaches[i], param[0], param[1], nbtaches);

		pvm_initsend(PvmDataDefault);
		pvm_pkint(&min, 1, 1);
		pvm_pkdouble(&etalement, 1, 1);
		pvm_pkint(&X, 1, 1);
		pvm_pkint(&valeur, 1, 1);
		pvm_send(numtaches[i], msgtype);
		
		// printf("Envoi %d effectué.\n",i);
	}
	
	remainingHosts = nhost;
	msgtype = MAITRE_RECOIT;
	// printf("Attente de réponse des host ... \n");
	while (remainingHosts) {
		pvm_recv(-1, msgtype);
		pvm_upkint(&confirmation, 1, 1);
		remainingHosts--;
		// printf("Confirmation reçue de tache n° %d. %d hosts restants.\n", confirmation, remainingHosts);
	}
	
	/* Début de la mesure */
	/*
	__clock_t begin = clock();
	*/
	initTimer;
	startTimer;

	int remainingLines = Y;
	msgtype = MAITRE_ENVOI;
	
	for (lineIndex=0 ; lineIndex < nhost ; lineIndex++) {
		pvm_initsend(PvmDataDefault);
		
		pvm_pkint(&lineIndex, 1, 1);
		pvm_pkint(image[lineIndex], X, 1);
		pvm_send(numtaches[lineIndex], msgtype);
	
		// printf("Envoi %d effectué. \n",lineIndex);
		remainingLines--;
	}
	
	lineIndex -= 1;
	
	int pidSender;
	int receivedIndexLine;
	// printf("\t DEBUT DU WHILE \n\n");
	while (remainingLines != 0) {
		// printf("En reception de ligne ... \n");
		receivedIndexLine = 0;
		msgtype = MAITRE_RECOIT;
		pvm_recv(-1, msgtype);
		pvm_upkint(&receivedIndexLine, 1, 1);
		pvm_upkint(&pidSender, 1, 1);
		pvm_upkint(resultat[receivedIndexLine], X, 1);

		// printf("Ligne n°%d reçue de tache n°%d.\n première ligne==%d\n", receivedIndexLine, pidSender, resultat[receivedIndexLine][0]);
		// TODO il reçoit des lignes mais ne renvoit pas de nouvelles lignes à
		// celui qui lui a envoyé !
		// Le master doit envoyer d'abord des lignes à sa liste de hosts,
		// puis attendre de recevoir des lignes et renvoyer une ligne à celui
		// qui lui a envoyé qqchose.
		// resultat[receivedIndexLine] = 1;
		remainingLines--;
		lineIndex++;
		
		// printf("lineIndex incrémenté à  ===%d.\n", lineIndex);
		// printf("image à cet index ===%d.\n\n\n", image[lineIndex][0]);
		
		msgtype = MAITRE_ENVOI;
		pvm_initsend(PvmDataDefault);
		pvm_pkint(&lineIndex, 1, 1);
		pvm_pkint(image[lineIndex], X, 1);
		pvm_send(pidSender, msgtype);
	}
	
	/* fin du compteur de temps */
	/*
	__clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	time_spent *= (double)100;
	printf("durée = %f", time_spent);
	*/
	
	stopTimer;
	
	
	
	// printf("\t APRES LE FOR \n\n");
	msgtype = MAITRE_ENVOI;
	int signalStop = -1;
	for (lineIndex2=0 ; lineIndex2 < nhost ; lineIndex2++) {
		pvm_initsend(PvmDataDefault);
		
		pvm_pkint(&signalStop, 1, 1);
		pvm_send(numtaches[lineIndex2], msgtype);
	
		// printf("Signal d'arrêt n° %d envoyé. \n", lineIndex2);
	}

	/*========================================================================*/
	/* Sauvegarde de l'image dans le fichier resultat			*/
	/*========================================================================*/
	
	n = 0;
	for (i = 0 ; i < Y ; i++) {
		for (j = 0 ; j < X ; j++) {
			
			fprintf(Dst,"%3d ",resultat[i][j]);
			n++;
			if (n == NBPOINTSPARLIGNES) {
				n = 0;
				fprintf(Dst, "\n");
			}
		}
	}
				
	fprintf(Dst,"\n");
	fclose(Dst);
	
	// printf("\n");
	// printf("exécution terminée.\n");
	
	pvm_exit();
//	return time_spent;
	return tpsCalcul;
	exit(0); 
}
